	package com.assign;
	
	import java.util.ArrayList;
	import java.util.List;
	
	class Cart {
		  private int id;
		  private List<Product> products;
		  private double total;
		  
		  public Cart(int id) {
		    this.id = id;
		    this.products = new ArrayList<Product>();
		    this.total = 0.0;
		  }
		  
		  public void addProduct(Product product) {
		    products.add(product);
		    total += product.getPrice();
		    if(product.decrement())
			    product.setProductQuantity(product.getProductQuantity()+1);
		    
		  }
		  
		  public void printTotal() {
		    System.out.println("Cart Total: Rs" + total);
		  }
		  
		  public void printDetails() {
		    System.out.println("Cart Details:");
		    for (Product product : products) {
		      System.out.println("Name: " + product.getName() + ", Price: Rs" + product.getPrice() + ", Quantity in Cart: " + product.getProductQuantity()+"\n");
		    }
		    
		    printTotal();
		  }
	
		}
