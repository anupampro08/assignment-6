package com.assign;

import java.util.ArrayList;
import java.util.List;

class Inventory {
	  private List<Product> products;
	  
	  public Inventory() {
	    products = new ArrayList<Product>();
	  }
	  
	  public void addProduct(Product product) {
	    products.add(product);
	  }
	  
	  public void printDetails() {
	    System.out.println("Inventory Details:");
	    for (Product product : products) {
	      System.out.println("Name: " + product.getName() + ", Brand: " + product.getBrand() + ", Qty Remaining:: " + product.getQty() + ", Price: Rs" + product.getPrice()+"\n");
	    }
	  }
	}

