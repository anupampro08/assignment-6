package com.assign;

class Product {
	  private String name;
	  private String brand;
	  private int qty;
	  private double price;
	  private int productQuantity=0;
	  
	  public Product(String name, String brand, int qty, double price) {
	    this.name = name;
	    this.brand = brand;
	    this.qty = qty;
	    this.price = price;
	
	  }
	  
	  public boolean decrement() {
		  if(qty<=0) {
			  System.out.println("Out of Stock");
			  return false;
		  }
	    qty--;
	    return true;
	  }
	  
	  public String getName() {
	    return name;
	  }
	  
	  public String getBrand() {
	    return brand;
	  }
	  
	  public int getQty() {
	    return qty;
	  }
	  
	  public double getPrice() {
	    return price;
	  }

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}
	}