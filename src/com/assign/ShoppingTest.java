package com.assign;

public class ShoppingTest {
	public static void main(String[] args) {
		Customer customer = new Customer("Anupam Adhikari", "anupam@example.com", "555-555-5555");
		Product p1 = new Product("Headphone", "JBL", 3, 1599.99);
		Product p2 = new Product("Iphone 6s", "Apple", 2, 899.99);
		Product p3 = new Product("Hair Trimmer", "Philips", 3, 799.99);
		Product p4 = new Product("Slim Laptop", "HP", 1, 799.99);
		Product p5 = new Product("Smart Bulb", "Philips", 2, 799.99);
		
		Inventory inventory = new Inventory();
		inventory.addProduct(p1);
		inventory.addProduct(p2);
		inventory.addProduct(p3);
		inventory.addProduct(p4);
		inventory.addProduct(p5);

		Cart cart = new Cart(1);
		cart.addProduct(p1);
		cart.addProduct(p2);
		cart.addProduct(p3);
		cart.addProduct(p4);
		cart.addProduct(p5);
		
		inventory.printDetails();
		cart.printDetails();

	}
}
